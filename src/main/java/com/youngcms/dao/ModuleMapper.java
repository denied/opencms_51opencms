package com.youngcms.dao;

import com.youngcms.bean.Module;
import com.youngcms.dao.base.MapperSupport;

public interface ModuleMapper extends MapperSupport<Module> {
}