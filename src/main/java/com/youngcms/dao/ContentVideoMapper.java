package com.youngcms.dao;

import com.youngcms.bean.ContentVideo;
import com.youngcms.dao.base.MapperSupport;

public interface ContentVideoMapper extends MapperSupport<ContentVideo> {
}