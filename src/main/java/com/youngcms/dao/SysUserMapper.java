package com.youngcms.dao;

import com.youngcms.bean.SysUser;
import com.youngcms.dao.base.MapperSupport;

public interface SysUserMapper extends MapperSupport<SysUser> {
}