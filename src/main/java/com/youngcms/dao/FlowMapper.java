package com.youngcms.dao;

import com.youngcms.bean.Flow;
import com.youngcms.dao.base.MapperSupport;

public interface FlowMapper extends MapperSupport<Flow> {
}