package com.youngcms.dao;

import com.youngcms.bean.SysUserRole;
import com.youngcms.dao.base.MapperSupport;

public interface SysUserRoleMapper extends MapperSupport<SysUserRole> {

	
}