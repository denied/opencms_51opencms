package com.youngcms.dao;

import com.youngcms.bean.ReptileRule;
import com.youngcms.dao.base.MapperSupport;

public interface ReptileRuleMapper extends MapperSupport<ReptileRule> {
}