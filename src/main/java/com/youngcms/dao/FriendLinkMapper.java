package com.youngcms.dao;

import com.youngcms.bean.FriendLink;
import com.youngcms.dao.base.MapperSupport;

public interface FriendLinkMapper extends MapperSupport<FriendLink> {
}