package com.youngcms.dao;

import com.youngcms.bean.ContentText;
import com.youngcms.dao.base.MapperSupport;

public interface ContentTextMapper extends MapperSupport<ContentText> {
}