package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.Advert;
import com.youngcms.dao.AdvertMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;

@Service
public class AdvertService extends ServiceSupport<Advert> {
	@Autowired
	private AdvertMapper advertMapper;

	@Override
	public MapperSupport<Advert> getMapperSupport() {
		return advertMapper;
	}
}
