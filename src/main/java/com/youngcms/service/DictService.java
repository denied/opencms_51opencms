package com.youngcms.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.Dict;
import com.youngcms.dao.DictMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class DictService extends ServiceSupport<Dict> {
   @Autowired
   private DictMapper dictMapper;

@Override
public MapperSupport<Dict> getMapperSupport() {
	// TODO Auto-generated method stub
	return dictMapper;
}
   
}
