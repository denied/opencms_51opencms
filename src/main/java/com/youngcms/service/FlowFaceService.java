package com.youngcms.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.FlowFace;
import com.youngcms.dao.FlowFaceMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class FlowFaceService extends ServiceSupport<FlowFace> {
   @Autowired
   private FlowFaceMapper flowFaceMapper;

@Override
public MapperSupport<FlowFace> getMapperSupport() {
	// TODO Auto-generated method stub
	return flowFaceMapper;
}
}
