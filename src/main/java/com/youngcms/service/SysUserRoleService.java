package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.SysUserRole;
import com.youngcms.dao.SysUserRoleMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;

@Service
public class SysUserRoleService extends ServiceSupport<SysUserRole> {
	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;

	@Override
	public MapperSupport<SysUserRole> getMapperSupport() {
		return sysUserRoleMapper;
	}

}
