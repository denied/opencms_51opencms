package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.Module;
import com.youngcms.dao.ModuleMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;

@Service
public class ModuleService extends ServiceSupport<Module> {
	@Autowired
	private ModuleMapper moduleMapper;

	@Override
	public MapperSupport<Module> getMapperSupport() {
		return moduleMapper;
	}
}
