package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.ContentOption;
import com.youngcms.dao.ContentOptionMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class ContentOptionService extends ServiceSupport<ContentOption> {

	@Autowired
	private ContentOptionMapper contentOptionMapper;

	@Override
	public MapperSupport<ContentOption> getMapperSupport() {
		return contentOptionMapper;
	}
	
	
}
