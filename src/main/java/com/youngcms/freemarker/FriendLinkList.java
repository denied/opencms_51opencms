package com.youngcms.freemarker;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.youngcms.bean.Advert;
import com.youngcms.bean.FriendLink;
import com.youngcms.service.FriendLinkService;
import com.youngcms.vo.QueryResult;

import freemarker.core.Environment;
import freemarker.ext.beans.ArrayModel;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
/**
 * 友情链接
 * @author fumiao-pc
 *
 */
@Repository
public class FriendLinkList implements TemplateDirectiveModel {
	
	@Autowired
	private FriendLinkService friendLinkService;

	@Override
	public void execute(Environment env, Map map, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		
		QueryResult<FriendLink> queryResult=friendLinkService.list();
		loopVars[0]=new ArrayModel(queryResult.getQueryResult().toArray(),new BeansWrapper());  
		body.render(env.getOut()); 
		
	}

}
