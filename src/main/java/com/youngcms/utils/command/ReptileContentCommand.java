package com.youngcms.utils.command;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.youngcms.bean.ReptileRule;
import com.youngcms.service.ContentReptileService;
import com.youngcms.service.ReptileRuleService;
import com.youngcms.utils.JacksonMapperUtil;
import com.youngcms.utils.reptile.Reptile;

/**
 * @author fumiao
 *
 * 抓取数据
 */
public class ReptileContentCommand extends BaseCommand{
	
	private  static Logger logger = LoggerFactory.getLogger(ReptileContentCommand.class);
	
	@Autowired
	private ReptileRuleService reptileRuleService;
	
	@Autowired
	private ContentReptileService contentReptileService;

	@Override
	public void execute(String params) {

		logger.info("==========抓取数据开始==============");
        try {
			JSONObject jsonObject=new JSONObject(params);
			Integer reptileRuleId=(Integer) jsonObject.get("reptileRuleId");
			ReptileRule reptileRule=reptileRuleService.selectByPrimaryKey(reptileRuleId);
			Reptile reptile=new Reptile(reptileRule,contentReptileService);
			Thread thread=new Thread(reptile);
			thread.start();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}

}
