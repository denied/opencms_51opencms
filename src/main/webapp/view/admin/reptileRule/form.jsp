<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
</head>
<body>
    <form:form action="${path}/admin/reptileRule/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
       <form:hidden path="id" />
	      <table class='formtable'>
	       <tr>
	          <td width="10%">名称</td>
	          <td><form:input path="name" cssClass="dfinput"/></td>
	          <td>编码格式</td>
	          <td><form:input path="coding" cssClass="dfinput"/>GBK或UTF-8等编码</td>
	       </tr>
	       <tr>
	          <td>采集数量</td>
	          <td><form:input path="number" cssClass="dfinput"/></td>
	          <td>时间格式</td>
	          <td><form:input path="timeFormat" cssClass="dfinput"/>例如yyyy-MM-dd HH:mm:ss</td>
	       </tr>
	       <tr>
	          <td>内容补全URL</td>
	          <td><form:input path="contentCompletionUrl" cssClass="dfinput"/></td>
	          <td>图片补全URL</td>
	          <td><form:input path="imageCompletionUrl" cssClass="dfinput"/></td>
	       </tr>
	        <tr>
	          <td>采集地址</td>
	          <td><form:textarea path="reptileUrl" cssClass="textinput"  /></td>
	       </tr>
	       </table>
            
	       <table class='formtable'>
	       <tr>
	          <td width="10%">&nbsp;</td>
	          <td>开始html</td>
	          <td>结束html</td>
	          <td>&nbsp;</td>
	       </tr>
	       <tr>
	          <td>内容集合</td>
	          <td><form:textarea path="contentAllStart" cssClass="textinput"  /></td>
	          <td><form:textarea path="contentAllEnd" cssClass="textinput"  /></td>
	          <td>&nbsp;</td>
	       </tr>
	       <tr>
	          <td>标题</td>
	          <td><form:textarea path="titleStart" cssClass="textinput"  /></td>
	          <td><form:textarea path="titleEnd" cssClass="textinput"  /></td>
	          <td>&nbsp;</td>
	       </tr>
	       <tr>
	          <td>关键字</td>
	          <td><form:textarea path="keywordsStart" cssClass="textinput"  /></td>
	          <td><form:textarea path="keywordsEnd" cssClass="textinput"  /></td>
	          <td>&nbsp;</td>
	       </tr>
	       <tr>
	          <td>描述</td>
	          <td><form:textarea path="descriptionStart" cssClass="textinput"  /></td>
	          <td><form:textarea path="descriptionEnd" cssClass="textinput"  /></td>
	          <td>&nbsp;</td>
	       </tr>
	       <tr>
	          <td>内容</td>
	          <td><form:textarea path="contentStart" cssClass="textinput"  /></td>
	          <td><form:textarea path="contentEnd" cssClass="textinput"  /></td>
	          <td>&nbsp;</td>
	       </tr>
	       <tr>
	          <td>发布时间</td>
	          <td><form:textarea path="publishTimeStart" cssClass="textinput"  /></td>
	          <td><form:textarea path="publishTimeEnd" cssClass="textinput"  /></td>
	          <td>&nbsp;</td>
	       </tr>
	       <tr>
	          <td>作者</td>
	          <td><form:textarea path="authorStart" cssClass="textinput"  /></td>
	          <td><form:textarea path="authorEnd" cssClass="textinput"  /></td>
	          <td>&nbsp;</td>
	       </tr>
	       <tr>
	          <td>来源</td>
	          <td><form:textarea path="sourceStart" cssClass="textinput"  /></td>
	          <td><form:textarea path="sourceEnd" cssClass="textinput"  /></td>
	          <td>&nbsp;</td>
	       </tr>
	       <tr>
	          <td>访问量</td>
	          <td><form:textarea path="readNumberStart" cssClass="textinput"  /></td>
	          <td><form:textarea path="readNumberEnd" cssClass="textinput"  /></td>
	          <td>&nbsp;</td>
	       </tr>
	       </table>
     </form:form>
</body>
</html>
