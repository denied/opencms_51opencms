<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<style type="text/css">
div#rMenu {position:absolute; visibility:hidden; top:0; background-color: #000;text-align: left;padding: 1px;}
div#rMenu ul li{
	margin: 1px 0;
	padding: 5px 10px;
	cursor: pointer;
	list-style: none outside none;
	background-color: #fff;
}
</style>
<script type="text/javascript">
var setting = {
    view: {
		dblClickExpand: dblClickExpand
	},
	async: {
		enable: true,
		url:path+"/admin/module/simpleNodes",
		autoParam:["id=parent_module_id"]
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback: {
		onClick: onClick,
		onRightClick: OnRightClick
	}
};
var node;
function OnRightClick(event, treeId, treeNode) {
	showRMenu(event.clientX, event.clientY);
	node=treeNode;
}
function showRMenu(x, y) {
	$("#rMenu ul").show();
	rMenu.css({"top":y+"px", "left":x+"px", "visibility":"visible"});
	$("body").bind("mousedown", onBodyMouseDown);
}
function hideRMenu() {
	if (rMenu) rMenu.css({"visibility": "hidden"});
	$("body").unbind("mousedown", onBodyMouseDown);
}
function onBodyMouseDown(event){
	if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
		rMenu.css({"visibility" : "hidden"});
	}
}
function addTreeNode() {
	hideRMenu();
    if(node.level == 0){
    	showForm('模块添加','${path}/admin/module/showForm','750','500');
	}else{
		showForm('模块添加','${path}/admin/module/showForm?parent_module_id='+node.id,'750','500');
	} 
	
}
function dblClickExpand(treeId, treeNode) {
	return treeNode.level > 0;
}

var zTree, rMenu;
$(document).ready(function(){
	$.fn.zTree.init($("#treeDemo"), setting);
	zTree = $.fn.zTree.getZTreeObj("treeDemo");
	rMenu = $("#rMenu");
});
function onClick(event, treeId, treeNode, clickFlag) {
	$("#iframepage").contents().find("#parent_module_id").val(treeNode.id);
	$("#iframepage").contents().find("#listForm").submit();
}	
</script>
</head>
<body>
   <form:form action="#" id="listForm" name="listForm">
	    <jsp:include page="/view/admin/common/place.jsp" />
	    <div class="rightinfo">
	    <div style="float: left;width: 12%;height: 88%;border-right: 1px solid #cbcbcb;">
			<ul id="treeDemo" class="ztree"></ul> 
		</div>
		<div id="rMenu">
			<ul>
				<li id="m_add" onclick="addTreeNode();">增加节点</li>
			</ul>
        </div>
		<div id="ifreamDiv"  style="float: left;width: 84%;margin: 0;padding: 0;">
			<iframe id="iframepage" src="${path }/admin/module/list" width="100%" height="90%"></iframe>
		</div>
	    </div>
    </form:form>
</body>
</html>
