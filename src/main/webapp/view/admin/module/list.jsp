<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
</head>
<body>
    <form:form action="#" id="listForm" name="listForm">
    <input type="hidden" value="${pageView.pId }" id="parent_module_id" name="pId"/>
    <div class="rightinfo" style="padding-top: 0;">
    <table class="tablelist">
    	<thead>
	    	<tr>
	        <th width="5%"><input type="checkbox" checked="checked"/></th>
	        <th width="5%">编号</th>
	        <th width="10%">模块名称</th>
	        <th width="20%">URL</th>
	        <th width="10%">是否显示</th>
	        <th width="10%">图标</th>
	        <th width="10%">TARGET</th>
	        <th width="10%">排序</th>
         <th width="10%">操作</th>	        </tr>
       </thead>
        <tbody>
        <c:forEach items="${pageView.pageDate }" var="bean">
	        <tr>
	        <td><input name="id" type="checkbox" value="${bean.id}" /></td>
	        <td>${bean.id }</td>
	        <td>${bean.name }</td>
	        <td>${bean.href }</td>
	        <td><showCnContent:getCityName id="${bean.moduleType }"></showCnContent:getCityName></td>
	        <td><i class="${bean.icon }"></i></td>
	        <td>${bean.target }</td>
	        <td>${bean.sort }</td>
	        <td class="td-manage">
		        <a title="编辑" href="javascript:showForm('用户修改','${path}/admin/module/showForm?id=${bean.id }','750','500')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6df;</i>
		        </a>
		        <a title="删除" href="javascript:del('${path }/admin/module/delete?id=${bean.id}')" class="ml-5" style="text-decoration: none">
		            <i class="Hui-iconfont">&#xe6e2;</i>
		        </a>
			</td>
	        </tr>
       </c:forEach>
        </tbody>
    </table>
   <jsp:include page="/view/admin/common/pageinfo.jsp" />
    </div>
    </form:form>
</body>
</html>
