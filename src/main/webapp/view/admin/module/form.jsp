<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp"%>
<%@ include file="/view/admin/common/import.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
var setting = {
		async : {
			enable: true,
			url:path+"/admin/module/simpleNodes",
			autoParam:["id=parent_module_id"]
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		check : {
			enable : true,
			chkStyle: "radio",
			radioType: "all"
		},
		callback : {
			onClick : onClick,
			onCheck : onCheck
		}
	};
	function onClick(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("treeDemo");
		zTree.checkNode(treeNode, !treeNode.checked, null, true);
		return false;
	}
	function onCheck(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
		nodes = zTree.getCheckedNodes(true),
		v = "";
		id = "";
		for (var i=0, l=nodes.length; i<l; i++) {
			v  = nodes[i].name ;
			id = nodes[i].id ;
		}
		$("#pName").attr("value", v);
		$("#pId").attr("value", id);
	}

	function showMenu() {
		var cityObj = $("#pName");
		var cityOffset = $("#pName").offset();
		$("#menuContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight()-10 + "px"}).slideDown("fast");

		$("body").bind("mousedown", onBodyDown);
	}
	function hideMenu() {
		$("#menuContent").fadeOut("fast");
		$("body").unbind("mousedown", onBodyDown);
	}
	function onBodyDown(event) {
		if (!(event.target.id == "menuBtn" || event.target.id == "pName" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
			hideMenu();
		}
	}
	$(document).ready(function(){
		 $.fn.zTree.init($("#treeDemo"), setting);
		 $('#btn-icon').iconpicker({
             rows: 3,
             cols: 10,
             align: 'left'
         });
		 $('#btn-icon').on('change', function(e) { 
			 var icon="glyphicon "+e.icon;
			 $("#icon").val(icon);
			});
	});
	
</script>
</head>
<body>
	<form:form action="${path}/admin/module/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
		<form:hidden path="id" />
		<table class="formtable">
			<tr>
				<td>父类模块</td>
				<td>
				    <input type="text"  id="pName" value="${pageForm.pName }" class="dfinput" onclick="showMenu()" />
				    <form:hidden path="pId" />
				</td>
				<td>模块名称</td>
				<td><form:input path="name" cssClass="dfinput" /></td>
			</tr>
			<tr>
				<td>URL</td>
				<td><form:input path="href" cssClass="dfinput" /></td>
				<td>TARGET</td>
				<td><form:input path="target" cssClass="dfinput" /></td>
			</tr>
			<tr>
				<td>排序</td>
				<td><form:input path="sort" cssClass="dfinput" /></td>
				<td>是否显示</td>
				<td>
				   <form:select path="moduleType" cssClass="select2">
                      <form:options items="${MODULE_TYPE_LIST }" itemValue="value" itemLabel="label" />
                   </form:select>
				</td>
			</tr>
			<tr>
			   <td>图标</td>
				<td colspan="3">
				 <form:input path="icon" cssClass="dfinput"/>
				 <div id="btn-icon"></div>
				</td>
		    </tr>
			</table>
	</form:form>
	<div id="menuContent">
	    <ul id="treeDemo" class="ztree ztreeSty"></ul>
 </div>
</body>
</html>
