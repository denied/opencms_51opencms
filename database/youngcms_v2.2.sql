/*
Navicat MySQL Data Transfer

Source Server         : 182.92.110.91
Source Server Version : 50544
Source Host           : 182.92.110.91:3306
Source Database       : cms_website

Target Server Type    : MYSQL
Target Server Version : 50544
File Encoding         : 65001

Date: 2016-07-12 22:41:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tb_advert`
-- ----------------------------
DROP TABLE IF EXISTS `tb_advert`;
CREATE TABLE `tb_advert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `click_num` varchar(255) DEFAULT NULL,
  `create_time` varchar(255) DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `advert_position_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3FE3E6C584811AC7` (`advert_position_id`),
  CONSTRAINT `FK3FE3E6C584811AC7` FOREIGN KEY (`advert_position_id`) REFERENCES `tb_advert_position` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_advert
-- ----------------------------
INSERT INTO `tb_advert` VALUES ('3', '0', '2016-07-06 20:33:25', '2016-06-22', '/upload/2016-07-06/1467808404780.jpg', '2016-06-22', '主页广告', '21212', '2');
INSERT INTO `tb_advert` VALUES ('4', '0', '2016-07-06 20:33:32', '2016-07-06', '/upload/2016-07-06/1467808412142.jpg', '2016-07-06', '主页横向广告位一', 'http://www.baidu.com', '4');
INSERT INTO `tb_advert` VALUES ('5', '0', '2016-07-06 20:33:40', '2016-07-06', '/upload/2016-07-06/1467808418972.jpg', '2016-07-06', '主页横向广告位二', 'http://www.baidu.com', '5');

-- ----------------------------
-- Table structure for `tb_advert_position`
-- ----------------------------
DROP TABLE IF EXISTS `tb_advert_position`;
CREATE TABLE `tb_advert_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` varchar(255) DEFAULT NULL,
  `height` varchar(255) DEFAULT NULL,
  `is_open` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `width` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_advert_position
-- ----------------------------
INSERT INTO `tb_advert_position` VALUES ('2', '2015-08-11 14:42:51', '250', '7', '主页图片切换', '10', '100%');
INSERT INTO `tb_advert_position` VALUES ('4', null, '80', '7', '主页横向广告位一', '9', '495');
INSERT INTO `tb_advert_position` VALUES ('5', null, '80', '7', '主页横向广告位二', '9', '495');

-- ----------------------------
-- Table structure for `tb_channel`
-- ----------------------------
DROP TABLE IF EXISTS `tb_channel`;
CREATE TABLE `tb_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `p_id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `sort` varchar(255) DEFAULT NULL,
  `is_nav` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2C14E43260131DAA` (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_channel
-- ----------------------------
INSERT INTO `tb_channel` VALUES ('1', '顶级栏目', null, 'root', '1', '2');
INSERT INTO `tb_channel` VALUES ('43', '课程学习', '1', 'course', '3', '1');
INSERT INTO `tb_channel` VALUES ('44', '下载中心', '1', 'download', '6', '1');
INSERT INTO `tb_channel` VALUES ('45', '关于我们', '1', 'about', '7', '1');
INSERT INTO `tb_channel` VALUES ('46', '编程语言', '43', 'course_1', '31', '2');
INSERT INTO `tb_channel` VALUES ('47', '系统运维', '43', 'course_2', '32', '2');
INSERT INTO `tb_channel` VALUES ('48', '网站首页', '1', 'index', '2', '1');
INSERT INTO `tb_channel` VALUES ('50', '图库', '1', 'pic', '4', '1');
INSERT INTO `tb_channel` VALUES ('51', '视频', '1', ' video', '5', '1');
INSERT INTO `tb_channel` VALUES ('52', '文库', '1', 'doc', '8', '1');
INSERT INTO `tb_channel` VALUES ('56', '设计制作', '43', 'course_3', '33', '2');
INSERT INTO `tb_channel` VALUES ('57', '数据库', '43', 'course_4', '34', '2');
INSERT INTO `tb_channel` VALUES ('58', '移动互联', '43', 'course_5', '35', '2');

-- ----------------------------
-- Table structure for `tb_channel_model`
-- ----------------------------
DROP TABLE IF EXISTS `tb_channel_model`;
CREATE TABLE `tb_channel_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_channel_model
-- ----------------------------
INSERT INTO `tb_channel_model` VALUES ('11', '43', '4');
INSERT INTO `tb_channel_model` VALUES ('12', '43', '5');
INSERT INTO `tb_channel_model` VALUES ('17', '46', '4');
INSERT INTO `tb_channel_model` VALUES ('18', '46', '5');
INSERT INTO `tb_channel_model` VALUES ('19', '46', '6');
INSERT INTO `tb_channel_model` VALUES ('20', '46', '7');
INSERT INTO `tb_channel_model` VALUES ('21', '48', '4');
INSERT INTO `tb_channel_model` VALUES ('22', '48', '5');
INSERT INTO `tb_channel_model` VALUES ('23', '48', '6');
INSERT INTO `tb_channel_model` VALUES ('24', '48', '7');

-- ----------------------------
-- Table structure for `tb_cms_content`
-- ----------------------------
DROP TABLE IF EXISTS `tb_cms_content`;
CREATE TABLE `tb_cms_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `short_title` varchar(255) DEFAULT NULL COMMENT '短标题',
  `click_num` int(11) DEFAULT '0' COMMENT '点击次数',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` int(11) DEFAULT NULL COMMENT '编辑者',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `img_url` varchar(255) DEFAULT NULL COMMENT '缩略图',
  `channel_id` int(11) DEFAULT NULL COMMENT '栏目',
  `flow_face_value` int(11) DEFAULT NULL COMMENT '工作流',
  `flow_value` int(11) DEFAULT NULL COMMENT '工作流',
  `tags` varchar(255) DEFAULT NULL COMMENT '标签',
  `source` varchar(255) DEFAULT NULL COMMENT '来源',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `start_time` varchar(255) DEFAULT NULL COMMENT '开始时间',
  `end_time` varchar(255) DEFAULT NULL COMMENT '结束时间',
  `title_color` varchar(255) DEFAULT NULL COMMENT '标题颜色',
  `is_bold` int(11) DEFAULT NULL COMMENT '是否加粗',
  `weight` int(11) DEFAULT NULL COMMENT '权重',
  `update_time` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `model_id` int(11) NOT NULL COMMENT '模型ID',
  PRIMARY KEY (`id`),
  KEY `FK794F3B32ECC8F437` (`channel_id`),
  CONSTRAINT `FK794F3B32ECC8F437` FOREIGN KEY (`channel_id`) REFERENCES `tb_channel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2051 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_cms_content
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_content`
-- ----------------------------
DROP TABLE IF EXISTS `tb_content`;
CREATE TABLE `tb_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `short_title` varchar(255) DEFAULT NULL COMMENT '短标题',
  `click_num` int(11) DEFAULT '0' COMMENT '点击次数',
  `create_time` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) DEFAULT NULL COMMENT '编辑者',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `img_url` varchar(255) DEFAULT NULL COMMENT '缩略图',
  `channel_id` int(11) DEFAULT NULL COMMENT '栏目',
  `flow_face_value` int(11) DEFAULT NULL COMMENT '工作流',
  `flow_value` int(11) DEFAULT NULL COMMENT '工作流',
  `tags` varchar(255) DEFAULT NULL COMMENT '标签',
  `source` varchar(255) DEFAULT NULL COMMENT '来源',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `start_time` varchar(255) DEFAULT NULL COMMENT '开始时间',
  `end_time` varchar(255) DEFAULT NULL COMMENT '结束时间',
  `title_color` varchar(255) DEFAULT NULL COMMENT '标题颜色',
  `is_bold` int(11) DEFAULT NULL COMMENT '是否加粗',
  `weight` int(11) DEFAULT NULL COMMENT '权重',
  `update_time` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `href` varchar(255) DEFAULT NULL COMMENT '外部链接',
  `is_top` int(11) DEFAULT '0' COMMENT '是否置顶',
  `is_comment` int(11) DEFAULT '0' COMMENT '是否允许评论',
  `model_id` int(11) DEFAULT NULL COMMENT '模型ID',
  `template` varchar(255) DEFAULT NULL COMMENT '模板',
  PRIMARY KEY (`id`),
  KEY `FK794F3B32ECC8F437` (`channel_id`),
  CONSTRAINT `tb_content_ibfk_1` FOREIGN KEY (`channel_id`) REFERENCES `tb_channel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2055 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_content
-- ----------------------------
INSERT INTO `tb_content` VALUES ('2052', '2121', '2121', '0', '2016-07-09 18:23:07', '超级管理员', '2121', '', '46', null, null, '2121', '2121', '2121', '', '', '2121', null, '10', null, null, null, '13', '15', null, null);
INSERT INTO `tb_content` VALUES ('2053', '2121', '2121', '0', '2016-07-09 18:23:44', '超级管理员', '2121', '', '46', null, null, '212121', '2121', '2121', '2016-07-09', '2016-07-09', '2121', null, '10', null, null, null, '13', '15', null, null);
INSERT INTO `tb_content` VALUES ('2054', '2121', '2121', '0', '2016-07-10 12:57:37', '超级管理员', '2121212', '/upload/2016-07-10/1468132285813.jpg;', '45', null, null, '212121', '2121', '21212', '', '', '#00cc66', null, '40', null, null, null, '14', '15', null, null);

-- ----------------------------
-- Table structure for `tb_content_image`
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_image`;
CREATE TABLE `tb_content_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `click_num` int(11) DEFAULT '0' COMMENT '点击次数',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片链接',
  `status` int(11) DEFAULT '1' COMMENT '状态',
  `content_id` int(11) DEFAULT NULL COMMENT '主表ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2054 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_content_image
-- ----------------------------
INSERT INTO `tb_content_image` VALUES ('2052', null, '0', '/upload/2016-07-10/1468134479038.jpg', '1', '2051');
INSERT INTO `tb_content_image` VALUES ('2053', null, '0', '/upload/2016-07-10/1468134479040.jpg', '1', '2051');

-- ----------------------------
-- Table structure for `tb_content_option`
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_option`;
CREATE TABLE `tb_content_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `click_num` int(11) DEFAULT '0' COMMENT '点击次数',
  `option_url` varchar(255) DEFAULT NULL COMMENT '附件链接',
  `status` int(11) DEFAULT '1' COMMENT '状态',
  `content_id` int(11) DEFAULT NULL COMMENT '主表ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2054 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_content_option
-- ----------------------------
INSERT INTO `tb_content_option` VALUES ('2052', null, '0', '/upload/2016-07-10/1468134479038.jpg', '1', '2051');
INSERT INTO `tb_content_option` VALUES ('2053', null, '0', '/upload/2016-07-10/1468134479040.jpg', '1', '2051');

-- ----------------------------
-- Table structure for `tb_content_text`
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_text`;
CREATE TABLE `tb_content_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `text` text COMMENT '内容',
  `content_id` int(11) DEFAULT NULL COMMENT '主表ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2054 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_content_text
-- ----------------------------
INSERT INTO `tb_content_text` VALUES ('2052', '/upload/2016-07-10/1468134479038.jpg', '2051');
INSERT INTO `tb_content_text` VALUES ('2053', '/upload/2016-07-10/1468134479040.jpg', '2051');

-- ----------------------------
-- Table structure for `tb_content_video`
-- ----------------------------
DROP TABLE IF EXISTS `tb_content_video`;
CREATE TABLE `tb_content_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `click_num` int(11) DEFAULT '0' COMMENT '点击次数',
  `video_url` varchar(255) DEFAULT NULL COMMENT '图片链接',
  `status` int(11) DEFAULT '1' COMMENT '状态',
  `length` int(11) DEFAULT '0' COMMENT '时长',
  `content_id` int(11) DEFAULT NULL COMMENT '主表ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2054 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_content_video
-- ----------------------------
INSERT INTO `tb_content_video` VALUES ('2052', null, '0', '/upload/2016-07-10/1468134479038.jpg', '1', '0', '2051');
INSERT INTO `tb_content_video` VALUES ('2053', null, '0', '/upload/2016-07-10/1468134479040.jpg', '1', '0', '2051');

-- ----------------------------
-- Table structure for `tb_dict`
-- ----------------------------
DROP TABLE IF EXISTS `tb_dict`;
CREATE TABLE `tb_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `dict_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA4F78AE750C5270A` (`dict_type_id`),
  CONSTRAINT `FKA4F78AE750C5270A` FOREIGN KEY (`dict_type_id`) REFERENCES `tb_dict_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dict
-- ----------------------------
INSERT INTO `tb_dict` VALUES ('3', '研究生', '1', '2');
INSERT INTO `tb_dict` VALUES ('4', '本科', '2', '2');
INSERT INTO `tb_dict` VALUES ('5', '文字', '1', '3');
INSERT INTO `tb_dict` VALUES ('6', '图片', '2', '3');
INSERT INTO `tb_dict` VALUES ('7', '是', '1', '4');
INSERT INTO `tb_dict` VALUES ('8', '否', '2', '4');
INSERT INTO `tb_dict` VALUES ('9', '图片', '1', '5');
INSERT INTO `tb_dict` VALUES ('10', '文字', '2', '5');
INSERT INTO `tb_dict` VALUES ('11', '菜单', '1', '6');
INSERT INTO `tb_dict` VALUES ('12', '功能', '2', '6');
INSERT INTO `tb_dict` VALUES ('13', '是', '1', '7');
INSERT INTO `tb_dict` VALUES ('14', '否', '2', '7');
INSERT INTO `tb_dict` VALUES ('15', '是', '1', '8');
INSERT INTO `tb_dict` VALUES ('16', '否', '2', '8');
INSERT INTO `tb_dict` VALUES ('17', '是', '1', '9');
INSERT INTO `tb_dict` VALUES ('18', '否', '2', '9');
INSERT INTO `tb_dict` VALUES ('19', '是', '1', '10');
INSERT INTO `tb_dict` VALUES ('20', '否', '2', '10');
INSERT INTO `tb_dict` VALUES ('21', '是', '1', '11');
INSERT INTO `tb_dict` VALUES ('22', '否', '2', '11');
INSERT INTO `tb_dict` VALUES ('23', '是', '1', '12');
INSERT INTO `tb_dict` VALUES ('24', '否', '2', '12');

-- ----------------------------
-- Table structure for `tb_dict_type`
-- ----------------------------
DROP TABLE IF EXISTS `tb_dict_type`;
CREATE TABLE `tb_dict_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ch_name` varchar(255) DEFAULT NULL,
  `en_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_dict_type
-- ----------------------------
INSERT INTO `tb_dict_type` VALUES ('2', '教育程度', 'education');
INSERT INTO `tb_dict_type` VALUES ('3', '友链类型', 'friendLinkType');
INSERT INTO `tb_dict_type` VALUES ('4', '是否开放', 'isOpen');
INSERT INTO `tb_dict_type` VALUES ('5', '广告位类型', 'advertPositionType');
INSERT INTO `tb_dict_type` VALUES ('6', '模块类型', 'moduleType');
INSERT INTO `tb_dict_type` VALUES ('7', '是否置顶', 'isTop');
INSERT INTO `tb_dict_type` VALUES ('8', '是否可以评论', 'isComment');
INSERT INTO `tb_dict_type` VALUES ('9', '是否有组图', 'hasGroupImages');
INSERT INTO `tb_dict_type` VALUES ('10', '是否有视频', 'hasVedio');
INSERT INTO `tb_dict_type` VALUES ('11', '是否有内容', 'hasContent');
INSERT INTO `tb_dict_type` VALUES ('12', '是否有附件', 'hasOptions');

-- ----------------------------
-- Table structure for `tb_flow`
-- ----------------------------
DROP TABLE IF EXISTS `tb_flow`;
CREATE TABLE `tb_flow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `is_open` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_flow
-- ----------------------------
INSERT INTO `tb_flow` VALUES ('1', '新闻审核', '1', '新闻审核', '1');
INSERT INTO `tb_flow` VALUES ('2', '会员审核', '1', '会员审核', '2');

-- ----------------------------
-- Table structure for `tb_flow_face`
-- ----------------------------
DROP TABLE IF EXISTS `tb_flow_face`;
CREATE TABLE `tb_flow_face` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `role_ids` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `role_names` varchar(255) DEFAULT NULL,
  `flow_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC9DE723DBCFBBABD` (`flow_id`),
  CONSTRAINT `FKC9DE723DBCFBBABD` FOREIGN KEY (`flow_id`) REFERENCES `tb_flow` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_flow_face
-- ----------------------------
INSERT INTO `tb_flow_face` VALUES ('2', '录入节点', '录入', '(1),(2)', '1', null, '超级管理员,管理员', '2');
INSERT INTO `tb_flow_face` VALUES ('3', '结束节点', '管理员审核', '(1),(2)', '2', null, '超级管理员,管理员', '2');
INSERT INTO `tb_flow_face` VALUES ('18', '3', '3', '(2),(14)', '2', null, '管理员,普通管理员', '1');

-- ----------------------------
-- Table structure for `tb_friend_link`
-- ----------------------------
DROP TABLE IF EXISTS `tb_friend_link`;
CREATE TABLE `tb_friend_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `click_num` varchar(255) DEFAULT NULL,
  `create_time` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_friend_link
-- ----------------------------
INSERT INTO `tb_friend_link` VALUES ('7', '0', '2015-08-07 18:18:36', '/upload/2016-06-23/1466691570458.jpg', '百度', '6', 'http://www.baidu.com');
INSERT INTO `tb_friend_link` VALUES ('11', null, null, '/upload/2016-07-06/1467786496079.jpg', '淘宝', '6', 'http://www.taobao.com');

-- ----------------------------
-- Table structure for `tb_model`
-- ----------------------------
DROP TABLE IF EXISTS `tb_model`;
CREATE TABLE `tb_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `temp_prefix` varchar(255) DEFAULT NULL COMMENT '模板前缀',
  `has_group_images` int(11) DEFAULT NULL COMMENT '是否有组图',
  `has_vedio` int(11) DEFAULT NULL COMMENT '是否有视频',
  `has_content` int(11) DEFAULT NULL COMMENT '是否有内容',
  `has_options` int(11) DEFAULT NULL COMMENT '时候有附件',
  `create_time` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_model
-- ----------------------------
INSERT INTO `tb_model` VALUES ('4', '新闻', 'news', '18', '20', '21', '24', '2016-07-12 16:50:57', null);
INSERT INTO `tb_model` VALUES ('5', '视频', 'vedio', '18', '19', '22', '24', '2016-07-12 16:51:42', null);
INSERT INTO `tb_model` VALUES ('6', '组图', 'image', '17', '20', '22', '24', '2016-07-12 16:52:08', null);
INSERT INTO `tb_model` VALUES ('7', '附件', 'option', '18', '20', '22', '23', '2016-07-12 16:52:29', null);

-- ----------------------------
-- Table structure for `tb_module`
-- ----------------------------
DROP TABLE IF EXISTS `tb_module`;
CREATE TABLE `tb_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `p_id` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL,
  `href` varchar(255) DEFAULT NULL,
  `module_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_module
-- ----------------------------
INSERT INTO `tb_module` VALUES ('1', '顶级模块', null, '1', 'rightFrame', '#', '11');
INSERT INTO `tb_module` VALUES ('6', '静态化', '1', '2', 'rightFrame', '#', '11');
INSERT INTO `tb_module` VALUES ('7', '系统管理', '1', '3', 'rightFrame', '#', '11');
INSERT INTO `tb_module` VALUES ('16', '首页静态化', '6', '21', 'rightFrame', '/admin/html/index', '11');
INSERT INTO `tb_module` VALUES ('17', '栏目静态化', '6', '22', 'rightFrame', '/admin/html/channel', '11');
INSERT INTO `tb_module` VALUES ('18', '模块管理', '7', '31', 'rightFrame', '/admin/module/dataPage', '11');
INSERT INTO `tb_module` VALUES ('19', '管理员列表', '7', '33', 'rightFrame', '/admin/sysUser/list', '11');
INSERT INTO `tb_module` VALUES ('20', '角色管理', '7', '32', 'rightFrame', '/admin/role/list', '11');
INSERT INTO `tb_module` VALUES ('23', '信息管理', '1', '1', 'rightFrame', '#', '11');
INSERT INTO `tb_module` VALUES ('24', '栏目管理', '23', '11', 'rightFrame', '/admin/channel/dataPage', '11');
INSERT INTO `tb_module` VALUES ('25', '内容管理', '23', '16', 'rightFrame', '/admin/content/dataPage', '11');
INSERT INTO `tb_module` VALUES ('26', '动态栏目树', '24', '13', 'rightFrame', '/admin/channel/nodes', '12');
INSERT INTO `tb_module` VALUES ('27', '列表', '24', '12', 'rightFrame', '/admin/channel/list', '12');
INSERT INTO `tb_module` VALUES ('28', '新闻列表', '25', '17', 'rightFrame', '/admin/cmsContent/list', '12');
INSERT INTO `tb_module` VALUES ('29', '栏目添加或修改页面', '24', '14', 'rightFrame', '/admin/channel/showForm', '12');
INSERT INTO `tb_module` VALUES ('30', '栏目添加或修改', '24', '15', 'rightFrame', '/admin/channel/addOrUpdate', '12');
INSERT INTO `tb_module` VALUES ('31', '新闻添加或修改页面', '25', '18', 'rightFrame', '/admin/cmsContent/showForm', '12');
INSERT INTO `tb_module` VALUES ('32', '新闻添加或修改', '25', '19', 'rightFrame', '/admin/cmsContent/addOrUpdate', '12');
INSERT INTO `tb_module` VALUES ('33', '静态栏目树', '24', '13', 'rightFrame', '/admin/channel/simpleNodes', '12');
INSERT INTO `tb_module` VALUES ('34', '系统维护', '1', '4', 'rightFrame', '#', '11');
INSERT INTO `tb_module` VALUES ('35', '实体列表', '34', '41', 'rightFrame', '/admin/code/list', '12');
INSERT INTO `tb_module` VALUES ('36', '字典维护', '34', '42', 'rightFrame', '/admin/dict/list', '11');
INSERT INTO `tb_module` VALUES ('37', '代码生成', '34', '41', 'rightFrame', '/admin/code/create', '12');
INSERT INTO `tb_module` VALUES ('38', '栏目删除', '24', '19', 'rightFrame', '/admin/channel/delete', '12');
INSERT INTO `tb_module` VALUES ('39', '新闻删除', '25', '20', 'rightFrame', '/admin/cmsContent/delete', '12');
INSERT INTO `tb_module` VALUES ('40', '字典类型', '34', '43', 'rightFrame', '/admin/dictType/list', '11');
INSERT INTO `tb_module` VALUES ('41', '字典类型添加或修改页面', '40', '44', 'rightFrame', '/admin/dictType/showForm', '12');
INSERT INTO `tb_module` VALUES ('42', '字典类型添加', '40', '45', 'rightFrame', '/admin/dictType/addOrUpdate', '12');
INSERT INTO `tb_module` VALUES ('43', '字典类型删除', '40', '45', 'rightFrame', '/admin/dictType/delete', '12');
INSERT INTO `tb_module` VALUES ('44', '字典添加或修改页面', '36', '46', 'rightFrame', '#', '12');
INSERT INTO `tb_module` VALUES ('45', '工作流', '34', '1', 'rightFrame', '/admin/flow/list', '11');
INSERT INTO `tb_module` VALUES ('47', '友情链接', '23', '1', 'rightFrame', '/admin/friendLink/list', '11');
INSERT INTO `tb_module` VALUES ('48', '广告位管理', '23', '1', 'rightFrame', '/admin/advertPosition/list', '11');
INSERT INTO `tb_module` VALUES ('49', '广告管理', '23', '1', 'rightFrame', '/admin/advert/list', '11');
INSERT INTO `tb_module` VALUES ('53', '资源管理', '23', '111', 'rightFrame', '/admin/template/dataPage', '11');
INSERT INTO `tb_module` VALUES ('54', '资源树状图', '53', '11', 'rightFrame', '/admin/template/file', '12');
INSERT INTO `tb_module` VALUES ('55', '模型管理', '34', '1', 'rightFrame', '/admin/model/list', '11');

-- ----------------------------
-- Table structure for `tb_role`
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `module_ids` varchar(255) DEFAULT NULL,
  `module_names` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES ('1', '超级管理员', '1,6,16,17,7,18,19,20,23,24,26,27,29,30,33,38,25,28,31,32,39,47,48,49,53,54,34,35,36,44,37,40,41,42,43,45,55', '顶级模块_静态化_首页静态化_栏目静态化_系统管理_模块管理_管理员列表_角色管理_信息管理_栏目管理_动态栏目树_列表_栏目添加或修改页面_栏目添加或修改_静态栏目树_栏目删除_内容管理_新闻列表_新闻添加或修改页面_新闻添加或修改_新闻删除_友情链接_广告位管理_广告管理_资源管理_资源树状图_系统维护_实体列表_字典维护_字典添加或修改页面_代码生成_字典类型_字典类型添加或修改页面_字典类型添加_字典类型删除_工作流_模型管理');
INSERT INTO `tb_role` VALUES ('2', '管理员', '1,6,16,17,7,18,19,20,23,24,26,27,29,30,33,38,25,28,31,32,39,47,48,49,34,35,36,44,37,40,41,42,43,45', '顶级模块_静态化_首页静态化_栏目静态化_系统管理_模块管理_管理员列表_角色管理_信息管理_栏目管理_动态栏目树_栏目列表_栏目添加或修改页面_栏目添加或修改_静态栏目树_栏目删除_文档管理_新闻列表_新闻添加或修改页面_新闻添加或修改_新闻删除_友情链接_广告位管理_广告管理_系统维护_实体列表_字典维护_字典添加或修改页面_代码生成_字典类型_字典类型添加或修改页面_字典类型添加_字典类型删除_工作流');
INSERT INTO `tb_role` VALUES ('14', '普通管理员', '1,6,16,17,7,18,19,20,23,24,26,27,29,30,33,38,25,28,31,32,39,47,48,49,34,35,36,44,37,40,41,42,43,45', '顶级模块_静态化_首页静态化_栏目静态化_系统管理_模块管理_管理员列表_角色管理_信息管理_栏目管理_动态栏目树_栏目列表_栏目添加或修改页面_栏目添加或修改_静态栏目树_栏目删除_文档管理_新闻列表_新闻添加或修改页面_新闻添加或修改_新闻删除_友情链接_广告位管理_广告管理_系统维护_实体列表_字典维护_字典添加或修改页面_代码生成_字典类型_字典类型添加或修改页面_字典类型添加_字典类型删除_工作流');

-- ----------------------------
-- Table structure for `tb_sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_user`;
CREATE TABLE `tb_sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` varchar(255) DEFAULT NULL,
  `last_login_time` varchar(255) DEFAULT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `real_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_sys_user
-- ----------------------------
INSERT INTO `tb_sys_user` VALUES ('3', '2015-07-24 18:15:13', '2016-07-12 22:19:47', 'admin', 'E10ADC3949BA59ABBE56E057F20F883E', '超级管理员');

-- ----------------------------
-- Table structure for `tb_sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_user_role`;
CREATE TABLE `tb_sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `sys_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKD37159673CB815BD` (`role_id`),
  KEY `FKD371596744326C32` (`sys_user_id`),
  CONSTRAINT `FKD37159673CB815BD` FOREIGN KEY (`role_id`) REFERENCES `tb_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKD371596744326C32` FOREIGN KEY (`sys_user_id`) REFERENCES `tb_sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_sys_user_role
-- ----------------------------
INSERT INTO `tb_sys_user_role` VALUES ('173', '1', '3');

-- ----------------------------
-- Table structure for `tb_user`
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` varchar(255) DEFAULT NULL,
  `last_login_time` varchar(255) DEFAULT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `real_name` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('4', '2015-07-23 16:15:53', null, 'admin', '7A99D273D2CABADAC6A5639DDA0803B7', '超级管理员', null, null, null);
